# RocksndiamondsLevels

Self made level set for the Rocks n diamonds game by Artsoft

Should be placed in

/home/user/.rocksndiamonds/levels/username/

As of 15-5-2019

12 Levels - complete

**HOW TO HELP WITH OR USE, THE LEVELS WITHIN THIS PROJECT**

1.  Set up an account on salsa.debian.org ( https://salsa.debian.org/help )
2.  fork the above project to your account (see above help)
3.  Install rocks and diamonds
on Debian this will be 

```
apt install rocksndiamonds
```
4. Cloning will create a directory so you can clone from your $HOME directory

```
git clone https://salsa.debian.org/zleap-guest/rocksndiamondslevels/$USERNAME
```
5. change to the repository directory

```
cd rocksndiamondslevels
```

Levels for rocksndiamonds can be found within the game directory structure at

```
/home/user/.rocksndiamonds/levels/usernane.
```

6. Copy **all** files, you can either do this from the terminal OR from the gui.
folders that are preceeded with a period .  are hidden, so within the file manager you may need to 
'''
show hidden files
'''
'
6a. From the terminal you can use
```
ls -la
```
to show hidden files

You need to copy both the 

001.level and levelinfo.conf files

to copy multiple files from the terminal use

cp *.level /pathtotarget/

if, at any time,  you feel lost as to where you are at the terminal you can use

pwd - print working directory

```
psutton@zleap:~/.rocksndiamonds/levels$ pwd
/home/psutton/.rocksndiamonds/levels
psutton@zleap:~/.rocksndiamonds/levels$ ls
custom2  psutton
psutton@zleap:~/.rocksndiamonds/levels$ 
```     

The above shows what I have on my system at home

7. You can now add more levels to this level set.
8. To push back to your repository (lets assume you have now got 013.level)
9. Copy these new files back to where you cloned to earlier
10. 
```
cd $HOME
cd  rocksndiamondslevels
```
and use
```
git add 013.level
git commit -m "add level 13"
git push
```
11. Enter your username and password for salsa and it should push changes to your repository